--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying(255) NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO postgres;

--
-- Name: user_data; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE user_data (
    did integer NOT NULL,
    tid integer NOT NULL,
    date timestamp with time zone,
    lastupdate integer,
    data text
);


ALTER TABLE public.user_data OWNER TO postgres;

--
-- Name: user_data_did_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_data_did_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_data_did_seq OWNER TO postgres;

--
-- Name: user_data_did_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_data_did_seq OWNED BY user_data.did;


--
-- Name: user_feedback; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE user_feedback (
    id bigint NOT NULL,
    tid integer,
    title character varying,
    text text,
    create_at integer
);


ALTER TABLE public.user_feedback OWNER TO postgres;

--
-- Name: TABLE user_feedback; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE user_feedback IS '用户信息反馈';


--
-- Name: COLUMN user_feedback.tid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_feedback.tid IS '用户tid';


--
-- Name: COLUMN user_feedback.text; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_feedback.text IS '信息内容';


--
-- Name: COLUMN user_feedback.create_at; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_feedback.create_at IS '反馈时间';


--
-- Name: user_feedback_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_feedback_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_feedback_id_seq OWNER TO postgres;

--
-- Name: user_feedback_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_feedback_id_seq OWNED BY user_feedback.id;


--
-- Name: user_infos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE user_infos (
    iid integer NOT NULL,
    tid integer,
    userinfo text,
    version character varying(50),
    pic character varying,
    lastupdatemark integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.user_infos OWNER TO postgres;

--
-- Name: TABLE user_infos; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE user_infos IS '用户帐号数据';


--
-- Name: COLUMN user_infos.tid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_infos.tid IS '关联user_token 表tid';


--
-- Name: COLUMN user_infos.userinfo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_infos.userinfo IS '用户信息';


--
-- Name: COLUMN user_infos.pic; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_infos.pic IS '图片字段';


--
-- Name: COLUMN user_infos.lastupdatemark; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_infos.lastupdatemark IS '用户信息最后上传时间';


--
-- Name: user_infos_iid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_infos_iid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_infos_iid_seq OWNER TO postgres;

--
-- Name: user_infos_iid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_infos_iid_seq OWNED BY user_infos.iid;


--
-- Name: user_qqhealth_api; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE user_qqhealth_api (
    id bigint NOT NULL,
    tid integer,
    access_token character varying,
    is_join smallint DEFAULT 0 NOT NULL,
    status smallint DEFAULT 1 NOT NULL,
    temp_table_id smallint DEFAULT 1 NOT NULL,
    lastupdatemark integer DEFAULT 0 NOT NULL,
    lastupdatemark_sleep integer DEFAULT 0 NOT NULL,
    status_sleep smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public.user_qqhealth_api OWNER TO postgres;

--
-- Name: COLUMN user_qqhealth_api.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_qqhealth_api.id IS '主键自增';


--
-- Name: COLUMN user_qqhealth_api.tid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_qqhealth_api.tid IS '用户tid';


--
-- Name: COLUMN user_qqhealth_api.access_token; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_qqhealth_api.access_token IS 'QQ登录后access_token';


--
-- Name: COLUMN user_qqhealth_api.is_join; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_qqhealth_api.is_join IS '是否参与QQ健康数据同步 1 参与 0 不参与，默认为0';


--
-- Name: COLUMN user_qqhealth_api.status; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_qqhealth_api.status IS '当前同步状态 1 有数据更新 0 无更新 ， 默认为 1';


--
-- Name: COLUMN user_qqhealth_api.temp_table_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_qqhealth_api.temp_table_id IS '临时qq上传数据表ID，默认为1';


--
-- Name: COLUMN user_qqhealth_api.lastupdatemark; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_qqhealth_api.lastupdatemark IS '最后数据上传时间点，默认为0，上传所有';


--
-- Name: COLUMN user_qqhealth_api.lastupdatemark_sleep; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_qqhealth_api.lastupdatemark_sleep IS 'QQ睡眠数据最后创建时间';


--
-- Name: COLUMN user_qqhealth_api.status_sleep; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_qqhealth_api.status_sleep IS '睡眠数据是否有更新标记';


--
-- Name: user_qqhealth_api_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_qqhealth_api_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_qqhealth_api_id_seq OWNER TO postgres;

--
-- Name: user_qqhealth_api_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_qqhealth_api_id_seq OWNED BY user_qqhealth_api.id;


--
-- Name: user_qqupload_temp_sleep; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE user_qqupload_temp_sleep (
    id bigint NOT NULL,
    tid integer,
    end_time integer DEFAULT 0 NOT NULL,
    start_time integer DEFAULT 0 NOT NULL,
    total_time integer DEFAULT 0 NOT NULL,
    light_sleep integer DEFAULT 0 NOT NULL,
    deep_sleep integer DEFAULT 0 NOT NULL,
    awake_time integer DEFAULT 0 NOT NULL,
    detail character varying
);


ALTER TABLE public.user_qqupload_temp_sleep OWNER TO postgres;

--
-- Name: COLUMN user_qqupload_temp_sleep.tid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_qqupload_temp_sleep.tid IS '用户tid';


--
-- Name: COLUMN user_qqupload_temp_sleep.end_time; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_qqupload_temp_sleep.end_time IS '睡眠结束的时间戳';


--
-- Name: COLUMN user_qqupload_temp_sleep.start_time; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_qqupload_temp_sleep.start_time IS '睡眠开始的时间戳';


--
-- Name: COLUMN user_qqupload_temp_sleep.total_time; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_qqupload_temp_sleep.total_time IS '今日睡眠总时间';


--
-- Name: COLUMN user_qqupload_temp_sleep.light_sleep; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_qqupload_temp_sleep.light_sleep IS '今日浅睡眠总时间';


--
-- Name: COLUMN user_qqupload_temp_sleep.deep_sleep; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_qqupload_temp_sleep.deep_sleep IS '今日深睡眠总时间';


--
-- Name: COLUMN user_qqupload_temp_sleep.awake_time; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_qqupload_temp_sleep.awake_time IS '今日睡眠期间醒来状态的总时间';


--
-- Name: COLUMN user_qqupload_temp_sleep.detail; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_qqupload_temp_sleep.detail IS '睡眠阶段详情数据';


--
-- Name: user_qqupload_temp_sleep_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_qqupload_temp_sleep_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_qqupload_temp_sleep_id_seq OWNER TO postgres;

--
-- Name: user_qqupload_temp_sleep_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_qqupload_temp_sleep_id_seq OWNED BY user_qqupload_temp_sleep.id;


--
-- Name: user_qqupload_temp_steps; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE user_qqupload_temp_steps (
    id bigint NOT NULL,
    tid integer,
    "time" integer DEFAULT 0 NOT NULL,
    steps integer DEFAULT 0 NOT NULL,
    calories integer DEFAULT 0 NOT NULL,
    distance integer DEFAULT 0 NOT NULL,
    duration integer DEFAULT 3600 NOT NULL
);


ALTER TABLE public.user_qqupload_temp_steps OWNER TO postgres;

--
-- Name: TABLE user_qqupload_temp_steps; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE user_qqupload_temp_steps IS 'qq数据同步，用户数据临时表1';


--
-- Name: COLUMN user_qqupload_temp_steps.tid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_qqupload_temp_steps.tid IS '用户ID';


--
-- Name: COLUMN user_qqupload_temp_steps."time"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_qqupload_temp_steps."time" IS '产生数据时间点';


--
-- Name: COLUMN user_qqupload_temp_steps.steps; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_qqupload_temp_steps.steps IS '步数';


--
-- Name: COLUMN user_qqupload_temp_steps.calories; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_qqupload_temp_steps.calories IS '卡路里';


--
-- Name: COLUMN user_qqupload_temp_steps.distance; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_qqupload_temp_steps.distance IS '运动距离';


--
-- Name: COLUMN user_qqupload_temp_steps.duration; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_qqupload_temp_steps.duration IS '本次运动的持续时间';


--
-- Name: user_qqupload_temp_steps_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_qqupload_temp_steps_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_qqupload_temp_steps_id_seq OWNER TO postgres;

--
-- Name: user_qqupload_temp_steps_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_qqupload_temp_steps_id_seq OWNED BY user_qqupload_temp_steps.id;


--
-- Name: user_thirdparty; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE user_thirdparty (
    id bigint NOT NULL,
    third_name character varying,
    token character varying,
    email character varying,
    status integer DEFAULT 1 NOT NULL,
    tid integer
);


ALTER TABLE public.user_thirdparty OWNER TO postgres;

--
-- Name: COLUMN user_thirdparty.third_name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_thirdparty.third_name IS '第三方应用名称';


--
-- Name: COLUMN user_thirdparty.token; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_thirdparty.token IS '第三方应用分配token';


--
-- Name: COLUMN user_thirdparty.email; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_thirdparty.email IS 'user email';


--
-- Name: COLUMN user_thirdparty.status; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_thirdparty.status IS '当前状态 1 正常   2 关闭或取消授权不可用状态 ， 默认 1';


--
-- Name: user_thirdparty_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_thirdparty_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_thirdparty_id_seq OWNER TO postgres;

--
-- Name: user_thirdparty_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_thirdparty_id_seq OWNED BY user_thirdparty.id;


--
-- Name: user_token; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE user_token (
    token character varying(255),
    email character varying(255),
    id integer NOT NULL,
    tid integer
);


ALTER TABLE public.user_token OWNER TO postgres;

--
-- Name: COLUMN user_token.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_token.id IS '主键自增';


--
-- Name: COLUMN user_token.tid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN user_token.tid IS '用户id';


--
-- Name: user_token_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_token_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_token_id_seq OWNER TO postgres;

--
-- Name: user_token_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_token_id_seq OWNED BY user_token.id;


--
-- Name: did; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_data ALTER COLUMN did SET DEFAULT nextval('user_data_did_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_feedback ALTER COLUMN id SET DEFAULT nextval('user_feedback_id_seq'::regclass);


--
-- Name: iid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_infos ALTER COLUMN iid SET DEFAULT nextval('user_infos_iid_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_qqhealth_api ALTER COLUMN id SET DEFAULT nextval('user_qqhealth_api_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_qqupload_temp_sleep ALTER COLUMN id SET DEFAULT nextval('user_qqupload_temp_sleep_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_qqupload_temp_steps ALTER COLUMN id SET DEFAULT nextval('user_qqupload_temp_steps_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_thirdparty ALTER COLUMN id SET DEFAULT nextval('user_thirdparty_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_token ALTER COLUMN id SET DEFAULT nextval('user_token_id_seq'::regclass);


--
-- Name: unique_tid; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_infos
    ADD CONSTRAINT unique_tid UNIQUE (tid);


--
-- Name: user_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_data
    ADD CONSTRAINT user_data_pkey PRIMARY KEY (did);


--
-- Name: user_feedback_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_feedback
    ADD CONSTRAINT user_feedback_pkey PRIMARY KEY (id);


--
-- Name: user_info_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_infos
    ADD CONSTRAINT user_info_pkey PRIMARY KEY (iid);


--
-- Name: user_qqhealth_api_pkey_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_qqhealth_api
    ADD CONSTRAINT user_qqhealth_api_pkey_id PRIMARY KEY (id);


--
-- Name: user_qqhealth_api_unique_access_token; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_qqhealth_api
    ADD CONSTRAINT user_qqhealth_api_unique_access_token UNIQUE (access_token);


--
-- Name: user_qqupload_temp_steps_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_qqupload_temp_sleep
    ADD CONSTRAINT user_qqupload_temp_steps_pkey PRIMARY KEY (id);


--
-- Name: user_qqupload_temp_steps_pkey_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_qqupload_temp_steps
    ADD CONSTRAINT user_qqupload_temp_steps_pkey_id PRIMARY KEY (id);


--
-- Name: user_thirdparty_pkey_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_thirdparty
    ADD CONSTRAINT user_thirdparty_pkey_id PRIMARY KEY (id);


--
-- Name: user_token_pkey_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_token
    ADD CONSTRAINT user_token_pkey_id PRIMARY KEY (id);


--
-- Name: user_token_unique_tid; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_token
    ADD CONSTRAINT user_token_unique_tid UNIQUE (tid);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: user_qqupload_temp_sleep_index_tid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX user_qqupload_temp_sleep_index_tid ON user_qqupload_temp_sleep USING btree (tid);


--
-- Name: user_qqupload_temp_steps_index_tid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX user_qqupload_temp_steps_index_tid ON user_qqupload_temp_steps USING btree (tid);


--
-- Name: user_qqupload_temp_steps_index_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX user_qqupload_temp_steps_index_time ON user_qqupload_temp_steps USING btree ("time");


--
-- Name: user_thirdparty_index_email; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX user_thirdparty_index_email ON user_thirdparty USING btree (email);


--
-- Name: user_thirdparty_index_token; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX user_thirdparty_index_token ON user_thirdparty USING btree (token);


--
-- Name: user_feedback_tid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_feedback
    ADD CONSTRAINT user_feedback_tid_fkey FOREIGN KEY (tid) REFERENCES user_token(tid);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

